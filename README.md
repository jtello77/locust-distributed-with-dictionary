# LOCUST DISTRIBUIDO (MAESTRO - ESCLAVO)

Este proyecto usa imágenes docker de LOCUST, la cual, es una herramienta para realizar test de estrés sobre una aplicación o servidor. 

La novedad de este proyecto es la utilización de diccionarios, ya sea para asignar credenciales o incluir datos sobre en las peticiones. Este diccionario tiene la capacidad de distribuirse (sin repetirse) sobre los distintos contenedores esclavos, para así ejecutar una porción especifica del diccionario y no todo completamente. La distribución de este diccionario se calcula tomando las direcciones IP internas de cada contenedor para calcular que porción del diccionario que le toca ejecutar.

## Requisitos

- Se debe tener instalado docker y docker compose
- Tener habilitado el puerto 8089
- Se debe asignar las IP's de forma consecutiva en todos los contenedores, ejemplo: x.x.x.1, x.x.x.2, x.x.x.3 ,etc.

## ¿Cómo usar?

Se debe tener en cuenta que los siguientes aspectos:

- Debe configurar el archivo ENV con los archivos de diccionario. Se requiere definir al menos dos variables de ambiente, donde siempre se debe tener ***NUMBER_NODES*** y (***FIRST_DICT*** ó ***SECOND_DICT***) de lo contrario, el archivo fallará.

- Si no se va a usar la variable de ambiente ***FIRST_DICT*** ó ***SECOND_DICT***, se debe borrar completamente del archivo ENV.

- Si decide utilizar las variables ***FIRST_DICT*** y ***SECOND_DICT***, debe ingresar el arhivo con su extensión como: rockyou.txt. Además, este archivo debe estar incluido en la carpeta donde se encuentra *locustfile.py*, para que sea leido por este mismo archivo.

- Cuando se defina ***NUMBER_NODES***, es necesario que sea un número y ese número debe ser mayor el número total de contenedores que se ejecutan (Maestro y todos Esclavos).

## EJECUCIÓN

    docker-compose up --scale worker=4

El código anterior ejecutará un contenedor maestro con 4 contenedores esclavos (5 en total).

**NOTA:**  Para este código el valor en la variable de ambiente ***NUMBER_NODES*** del archivo **ENV** debe ser 5.
