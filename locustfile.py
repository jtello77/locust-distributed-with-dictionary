from locust import HttpUser, between, task
import socket
import os


def read_env_variables():
    try: 
        number_nodes = int(os.environ.get('NUMBER_NODES'))  # Set the numbers of container or instance 
        first_file_name = os.environ.get('FIRST_DICT') # Set the first file to read
        second_file_name = os.environ.get('SECOND_DICT') # Set the second file to read
        if number_nodes is None:
            number_nodes = 0
        if first_file_name is None and second_file_name is None:
            raise Exception('include at least one dictionary file')
        print('Las variables de ambiente son:', number_nodes, first_file_name, second_file_name)
    except Exception as error:
        print('We have an error during execution:',error)
        print('Dont\'t forget set the values in ENV file.')
        print('Include the dictionary files to be used in the same folder.')
        print('An example to use is:\nFIRST_DICT=rockyou.txt\SECOND_DICT=names.txt\nNUMBER_NODES=3')
        exit()
    else: 
        return number_nodes, first_file_name, second_file_name

def calc_ip_module(lines_number_document, number_nodes):
    """
    This line get container IPv4, and execute a module and division over the last IP number
    params: lines_number_document is the total lines of the document readed
    return: initial_line and final_line, is the section of the document
    """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        internal_ip = s.getsockname()[0]
        print('----------> Container Internal IP:', internal_ip)
        s.close()
        list_ip = internal_ip.split('.')
        last_number = int(list_ip[3])
        sector = last_number % number_nodes
        initial_line = sector * (lines_number_document / number_nodes)
        final_line = initial_line + (lines_number_document / number_nodes)
    except Exception as error:
        print('We have an error during cal_ip_module:',error)
    else:
        return int(initial_line), int(final_line)

def open_dict(file_name, number_nodes):
    """
    This line make a new list with data to proccess
    return: list_to_process
    """
    list_to_proccess = []
    try:        
        with open(file_name, 'r') as dictionary:
            line = dictionary.readlines()
            number_of_lines = len(line)
            initial_line, final_line = calc_ip_module(number_of_lines, number_nodes) # SET THE NUMBER OF LINE IN DOCUMENT
            for i in range(initial_line, final_line):
                list_to_proccess.append((line[i].split('\n'))[0])
    except Exception as error:
        print('We have an error during execution:', error)
    else:
        return list_to_proccess

class WebsiteUser(HttpUser):
    number_nodes, first_file_name, second_file_name = read_env_variables()
    wait_time = between(5, 15)
    
    #print(os.getcwd())
    if first_file_name is not None:
        list_firts_dict = open_dict(first_file_name, number_nodes)
    if second_file_name is not None:
        list_second_dict = open_dict(second_file_name, number_nodes)

    print('Primera lista', list_firts_dict)
    print('Segunda List:', list_second_dict)
    def on_start(self):
        self.client.post("/", {
            "username": "test_user",
            "password": ""
        })
    
    @task
    def index(self):
        self.client.get("/")
        self.client.get("/static/assets.js")
        
    @task
    def about(self):
        self.client.get("/about/")